<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Первая страницв</title>

<!--        <link rel="import" href="webapp/WEB-INF/include/header.html">-->

        <link href="webapp/resources/css/bootstrap.min.css" rel="stylesheet">
        <link href="webapp/resources/resources/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="webapp/resources/css/common.css" rel="stylesheet">

        <script src="webapp/resources/js/lib/jquery-2.1.4.min.js"></script>
        <script src="webapp/resources/js/lib/bootstrap.min.js"></script>
        <script src="webapp/resources/js/lib/json2.js"></script>
        <script src="webapp/resources/js/lib/util.js"></script>

    </head>

    <body>

        <div class="container">

            <?php
                include ("webapp/WEB-INF/include/menu-top.html");
            ?>

            <div class="row">
                <div class="col-md-3"></div>

                <div class="col-md-6">

                    <h1>Моя первая страница на php</h1>

                    <p>Вот так я сделал свою первую страничку на винде использвуя php</p>
                    <p>А следующий php скрипт считает сумму 2-х чисел:</p>

                    <?php
                        include "summ.php";
                    ?>

                    <br>
                    <br>

                    <button class="btn btn-danger center-block">Кнока, которую опасно нажимать</button>

                </div>

                <div class="col-md-3"></div>
            </div>
        </div>

        <?php
            include ("webapp/WEB-INF/include/menu-bottom.html");
        ?>

    </body>
</html>
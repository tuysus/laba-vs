/**
 * Created by Евгений on 30.11.2015.
 */

$(document).ready(function() {

//подсвечивать ссылку активную
    var fullHref = document.location.href;
    var href = fullHref.substr(fullHref.lastIndexOf("/") + 1, fullHref.length - fullHref.lastIndexOf("/") - 1);
    if (href != "")
        $(".navbar-collapse ul li a[href^='" + href + "']").css("background-color", "#17468d");
});
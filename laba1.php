<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Лаба 1 - ДИЗСС</title>

<!--    <link rel="import" href="webapp/WEB-INF/include/header.html">-->

    <link href="webapp/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="webapp/resources/resources/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="webapp/resources/css/common.css" rel="stylesheet">

    <script src="webapp/resources/js/lib/jquery-2.1.4.min.js"></script>
    <script src="webapp/resources/js/lib/bootstrap.min.js"></script>
    <script src="webapp/resources/js/lib/json2.js"></script>
    <script src="webapp/resources/js/lib/util.js"></script>

    <script type="text/javascript" src="webapp/resources/js/page/laba1.js"></script>

</head>

<body>

<div class="container">

    <?php
        include ("webapp/WEB-INF/include/menu-top.html");
    ?>

    <h3 class="text-center">Лабораторная 1 - ДИЗСС</h3>


        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#transfer" aria-controls="transfer" role="tab" data-toggle="tab">Перевод</a></li>
            <li role="presentation"><a href="#addition" aria-controls="addition" role="tab" data-toggle="tab">Сложение</a></li>
            <li role="presentation"><a href="#multiplication" aria-controls="multiplication" role="tab" data-toggle="tab">Умножение</a></li>
        </ul>

        <div class="tab-content">

            <div role="tabpane" class="tab-pane fade in active" id="transfer">

                <br>

                <div class="row">

                    <div class="col-md-3"></div>

                    <div class="col-md-6">
                        <form class="form-group">
                            <label for="bits" class="col-md-6 text-right control-label">Колличество разрядов:</label>
                            <div class="col-md-2">
                                <input id="bits" class="form-control text-center" type="text">
                            </div>
                        </form>
                    </div>

                    <div class="col-md-3"></div>

                </div>

                <hr>

                <div class="row">

                    <div class="col-md-6">
                        <label for="count1" class="col-md-12 control-label text-center">Число 1 :</label>
                        <form class="form-horizontal">
                            <div class="input-group">
                                <div class="input-group-addon">0.</div>
                                <input id="count1" class="form-control" type="text">
                            </div>
                        </form>

                        <div class="modal-footer">
                            <button id="btnSentFirstCount" type="button" data-dismiss="modal" class="btn btn-primary center-block">Преобразовать</button>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label for="count2" class="col-md-12 control-label text-center">Число 2 :</label>
                        <form class="form-horizontal">
                            <div class="input-group">
                                <div class="input-group-addon">0.</div>
                                <input id="count2" class="form-control" type="text">
                            </div>
                        </form>

                        <div class="modal-footer">
                            <button id="btnSentSecondCount" type="button" data-dismiss="modal" class="btn btn-primary center-block">Преобразовать</button>
                        </div>
                    </div>

                </div>

            </div>

            <div role="tabpane" class="tab-pane fade" id="addition">

            </div>

            <div role="tabpane" class="tab-pane fade" id="multiplication">

            </div>

        </div>
</div>

    <?php
        include ("webapp/WEB-INF/include/menu-bottom.html");
    ?>

</body>
</html>